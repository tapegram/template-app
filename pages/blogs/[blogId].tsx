import * as T from "../../types";
import Layout from "../../components/Layout";
import Post from "../../components/Post";
import { fetchLikesForPost } from "../../server/data/fauna/fauna";

type Props = {
  blog: T.Blog;
};

const BlogPage = (props: Props) => (
  <Layout title="About | Next.js + TypeScript Example">
    <h1>{props.blog.id}</h1>
    {props.blog.posts.map(toPost)}
  </Layout>
);

export async function getServerSideProps(context: any) {
  const blog = await fetchBlog(context.params.blogId);
  return {
    props: {
      blog,
    },
  };
}

/*
Eventually we will replace this with a real call to a DB or Service
*/
const fetchBlog = async (blogId: string): Promise<T.Blog> => ({
  id: blogId,
  posts: [
    T.toPost(
      "123",
      "Bonjour, Monde",
      "Body goes here",
      await fetchLikesForPost("123")
    ),
    T.toPost(
      "456",
      "Hello, World",
      "Body goes here",
      await fetchLikesForPost("456")
    ),
  ],
});

const toPost = (post: T.Post) => <Post key={post.id} post={post} />;

export default BlogPage;
