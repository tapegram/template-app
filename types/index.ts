export type Blog = {
  id: string;
  posts: Post[];
};

export type Post = {
  id: string;
  title: string;
  body: string;
  likes: number;
};

export const toPost = (
  id: string,
  title: string,
  body: string,
  likes: number
): Post => ({
  id,
  title,
  body,
  likes,
});
