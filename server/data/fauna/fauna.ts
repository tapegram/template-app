import { initializeApollo } from "./apolloClient";
import { gql } from "@apollo/client";

export const ALL_LIKES = gql`
  query {
    allLikes {
      data {
        postId
      }
    }
  }
`;

export const CREATE_LIKE = gql`
  mutation CreateLike($postId: String!) {
    createLike(data: { postId: $postId }) {
      _id
      postId
    }
  }
`;

export const fetchLikesForPost = async (postId: string): Promise<number> => {
  const apolloClient = initializeApollo();
  const { data } = await apolloClient.query({
    query: ALL_LIKES,
  });
  return findLikesForPost(postId, data.allLikes.data);
};

export const createLike = async (postId: string): Promise<Boolean> => {
  const apolloClient = initializeApollo();
  await apolloClient.mutate({
    mutation: CREATE_LIKE,
    variables: { postId: postId },
  });
  return true;
};

const findLikesForPost = (postId: string, likes: any): number =>
  likes.filter((like: any) => like.postId == postId).length;
