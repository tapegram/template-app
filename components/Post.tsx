import { useState } from "react";
import { createLike } from "../server/data/fauna/fauna";
import * as T from "../types";

type Props = {
  post: T.Post;
};

const Post = (props: Props) => {
  const [likes, setLikes] = useState(props.post.likes);
  return (
    <>
      <h1>Title: {props.post.title}</h1>
      <h3>Likes {likes} </h3>
      <button
        onClick={() => {
          createLike(props.post.id);
          setLikes(likes + 1);
        }}
      >
        Add Like
      </button>
      <p>{props.post.body}</p>
    </>
  );
};

export default Post;
